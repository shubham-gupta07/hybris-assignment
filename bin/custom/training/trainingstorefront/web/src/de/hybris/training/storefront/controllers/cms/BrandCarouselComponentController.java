package de.hybris.training.storefront.controllers.cms;


import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.training.core.model.BrandCarouselComponentModel;
import de.hybris.training.facades.training.BrandCarouselComponentFacade;
import de.hybris.training.storefront.controllers.ControllerConstants;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Controller("BrandCarouselComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.BrandCarouselComponentController)

public class BrandCarouselComponentController extends AbstractAcceleratorCMSComponentController<BrandCarouselComponentModel> {

    protected static final List<ProductOption> PRODUCT_OPTIONS = Arrays.asList(ProductOption.BASIC, ProductOption.PRICE);

    @Resource(name = "productSearchFacade")
    private ProductSearchFacade<ProductData> productSearchFacade;
    @Resource(name = "brandCarouselComponentFacade")
    private BrandCarouselComponentFacade brandCarouselComponentFacade;
    @Override
    protected void fillModel(HttpServletRequest request, Model model, BrandCarouselComponentModel component) {
        final List<ProductData> products = new ArrayList<>();

        products.addAll(collectLinkedProducts(component));
        products.addAll(collectSearchProducts(component));

        model.addAttribute("title", component.getTitle());
        model.addAttribute("productData", products);

    }

    protected List<ProductData> collectLinkedProducts(final BrandCarouselComponentModel component) {
        return brandCarouselComponentFacade.collectProducts(component);
    }

    protected List<ProductData> collectSearchProducts(final BrandCarouselComponentModel component) {
        final SearchQueryData searchQueryData = new SearchQueryData();
        searchQueryData.setValue(component.getSearchQuery());
        final String categoryCode = component.getCategoryCode();

        if (searchQueryData.getValue() != null && categoryCode != null) {
            final SearchStateData searchState = new SearchStateData();
            searchState.setQuery(searchQueryData);

            final PageableData pageableData = new PageableData();
            pageableData.setPageSize(100); // Limit to 100 matching results

            return productSearchFacade.categorySearch(categoryCode, searchState, pageableData).getResults();
        }

        return Collections.emptyList();
    }
}
