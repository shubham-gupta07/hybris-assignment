//package de.hybris.training.storefront.controllers.pages;
//
//import de.hybris.platform.acceleratorfacades.futurestock.FutureStockFacade;
//import de.hybris.platform.acceleratorservices.controllers.page.PageType;
//import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
//import de.hybris.platform.acceleratorstorefrontcommons.forms.ReviewForm;
//import de.hybris.platform.acceleratorstorefrontcommons.util.MetaSanitizerUtil;
//import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
//import de.hybris.platform.cms2.model.pages.ContentPageModel;
//
//import de.hybris.platform.commercefacades.product.ProductOption;
//import de.hybris.platform.commercefacades.product.data.ProductData;
//import de.hybris.platform.util.Config;
//import de.hybris.training.core.service.CustomProductService;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//import javax.annotation.Resource;
//import javax.inject.Scope;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.UnsupportedEncodingException;
//import java.util.Arrays;
//import java.util.List;
//
//@Controller
//@RequestMapping("/brand")
//
//public class BrandPageController extends AbstractPageController {
//    private static final String BRAND_CMS_PAGE = "cMSPageContent";
//    @Resource(name = "customProductService")
//    private CustomProductService customProductService;
//    @RequestMapping(method = RequestMethod.GET)
//    @RequestMapping(method = RequestMethod.GET)
//    public String productDetail(@PathVariable("productCode") final String encodedProductCode, final Model model,
//                                final HttpServletRequest request, final HttpServletResponse response)
//            throws CMSItemNotFoundException, UnsupportedEncodingException
//    {
//        final String productCode = decodeWithScheme(encodedProductCode, UTF_8);
//        final List<ProductOption> extraOptions = Arrays.asList(ProductOption.VARIANT_MATRIX_BASE, ProductOption.VARIANT_MATRIX_URL,
//                ProductOption.VARIANT_MATRIX_MEDIA);
//
//        final ProductData productData = productFacade.getProductForCodeAndOptions(productCode, extraOptions);
//
//        final String redirection = checkRequestUrl(request, response, productDataUrlResolver.resolve(productData));
//        if (StringUtils.isNotEmpty(redirection))
//        {
//            return redirection;
//        }
//
//
//        updatePageTitle(productCode, model);
//
//
//        populateProductDetailForDisplay(productCode, model, request, extraOptions);
//
//        model.addAttribute(new ReviewForm());
//        model.addAttribute("pageType", PageType.PRODUCT.name());
//        model.addAttribute("futureStockEnabled", Boolean.valueOf(Config.getBoolean(FUTURE_STOCK_ENABLED, false)));
//
//        final String metaKeywords = MetaSanitizerUtil.sanitizeKeywords(productData.getKeywords());
//        final String metaDescription = MetaSanitizerUtil.sanitizeDescription(productData.getDescription());
//        setUpMetaData(model, metaKeywords, metaDescription);
//        return getViewForPage(model);
//}
