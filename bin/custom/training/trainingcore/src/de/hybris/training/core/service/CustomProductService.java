package de.hybris.training.core.service;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.ProductService;

import java.util.List;

public interface CustomProductService extends ProductService {
    List<ProductModel> findAllProductsWhichAreNew();
    CategoryModel findCategoryByCategoryCode(String code);
    List<ProductModel> getProductsByBrand(String brand);
}
