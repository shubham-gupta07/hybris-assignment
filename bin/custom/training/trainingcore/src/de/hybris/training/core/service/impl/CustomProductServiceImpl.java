package de.hybris.training.core.service.impl;

import de.hybris.platform.category.model.CategoryModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.product.impl.DefaultProductService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.training.core.dao.CustomProductDAO;
import de.hybris.training.core.service.CustomProductService;

import java.util.List;

public class CustomProductServiceImpl extends DefaultProductService implements CustomProductService {
    private CustomProductDAO customProductDAO;

    @Override
    public List<ProductModel> findAllProductsWhichAreNew() {
        final List<ProductModel> result = customProductDAO.findAllProductsWhichAreNew();
            if (result.isEmpty()) {
                throw new UnknownIdentifierException("Product are Change to True");
            }
            return result;
        }

    @Override
    public CategoryModel findCategoryByCategoryCode(String code) {
        final CategoryModel result = customProductDAO.findCategoryByCategoryCode(code);
        if (result==null) {
            throw new UnknownIdentifierException("No Category associated with this "+code);
        }
        return result;

    }


    @Override
    public List<ProductModel> getProductsByBrand(String brand) {
        final List<ProductModel> result = customProductDAO.getProductsByBrand(brand);
        if (result == null) {
            throw new UnknownIdentifierException("No Product associated with this " + brand);
        }
        return result;
        }

    public CustomProductDAO getCustomProductDAO() {
        return customProductDAO;
    }

    public void setCustomProductDAO(CustomProductDAO customProductDAO) {
        this.customProductDAO = customProductDAO;
    }
}

