/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 12-Jan-2021, 2:50:16 PM                     ---
 * ----------------------------------------------------------------
 */
package org.trainingbackoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedYacceleratorbackofficeConstants
{
	public static final String EXTENSIONNAME = "trainingbackoffice";
	
	protected GeneratedYacceleratorbackofficeConstants()
	{
		// private constructor
	}
	
	
}
