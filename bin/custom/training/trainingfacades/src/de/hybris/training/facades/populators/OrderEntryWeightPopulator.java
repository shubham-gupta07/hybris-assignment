package de.hybris.training.facades.populators;

import de.hybris.platform.commercefacades.order.converters.populator.OrderEntryPopulator;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;

public class OrderEntryWeightPopulator extends OrderEntryPopulator {
    @Override
    public void populate(final AbstractOrderEntryModel source, final OrderEntryData target) {
        super.populate(source, target);
        addtotalWeight(source, target);
    }

    public void addtotalWeight(final AbstractOrderEntryModel orderEntry, final OrderEntryData entry) {
        if (orderEntry.getTotalWeight() != null) {
          entry.setTotalWeight(orderEntry.getTotalWeight());
        }
    }
}
