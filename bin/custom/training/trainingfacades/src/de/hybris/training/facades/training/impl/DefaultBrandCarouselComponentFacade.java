package de.hybris.training.facades.training.impl;

import de.hybris.platform.acceleratorfacades.productcarousel.impl.DefaultProductCarouselFacade;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.training.core.model.BrandCarouselComponentModel;
import de.hybris.training.core.service.CustomProductService;
import de.hybris.training.facades.training.BrandCarouselComponentFacade;

import java.util.List;

public class DefaultBrandCarouselComponentFacade extends DefaultProductCarouselFacade implements BrandCarouselComponentFacade {

    CustomProductService customProductService;

    public CustomProductService getCustomProductService() {
        return customProductService;
    }

    public void setCustomProductService(CustomProductService customProductService) {
        this.customProductService = customProductService;
    }

    @Override
    public List<ProductData> collectProducts(final BrandCarouselComponentModel component) {
        {
            component.setProducts(customProductService.getProductsByBrand(component.getBrand()));
            if (!isPreview())
            {
                return fetchProductsForNonPreviewMode(component);
            }
            else
            {
                return fetchProductsForPreviewMode(component);
            }
        }

    }
}
