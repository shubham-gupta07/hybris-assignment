package de.hybris.training.facades.training;

import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.training.core.model.BrandCarouselComponentModel;

import java.util.List;

public interface BrandCarouselComponentFacade {
    List<ProductData> collectProducts(final BrandCarouselComponentModel component);
}
